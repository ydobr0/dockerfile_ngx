FROM debian:9 as ass
RUN apt update && apt install wget gcc make libpcre3-dev zlib1g zlib1g-dev libssl-dev -y
RUN wget  https://nginx.org/download/nginx-1.10.0.tar.gz&&tar xvzf nginx-1.10.0.tar.gz && cd nginx-1.10.0 && ./configure && make && make install

FROM debian:9
COPY --from=ass /usr/lib/x86_64-linux-gnu/libcrypto.so.1.1 /usr/lib/x86_64-linux-gnu
WORKDIR /usr/local/nginx/sbin
COPY --from=ass /usr/local/nginx/sbin/nginx .
RUN mkdir ../html ../logs ../conf && touch ../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]
